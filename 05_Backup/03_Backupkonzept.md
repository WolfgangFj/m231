# Backupkonzept -  Ich brauche ein Backup - Aber wovon und wie?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben mithilfe des persönlichen Ablagekonzeptes ein einfaches Backupkonzept erstellt |

Im Thema Ablagekonzept haben Sie unter anderem analysiert, an welchen Orten Sie persönliche Daten abspeichern. Nun geht es darum zu identifizieren, bei welchen Daten Sie keinen Verlust in Kauf nehmen möchten und wie Sie für diese Daten eine Datensicherung erstellen können. 

## Aufgabenstellung
Erstellen Sie eine Tabelle, um folgende Fragen zu beantworten:

  **Szenario: Verlust des persönliches Smartphones.**
  - Welche Funktionen / Services können beim Verlust des persönlichen Smartphones nicht mehr verwendet werden (Einzelne Funktionen, Apps auflisten)?
  - Welche Daten sind beim Verlust betroffen?
  - Wo liegt das Backup der Daten?
  - Wie können die Daten wiederhergestellt werden?

**Beispiel:**
| Service  | Funktion                                                                         | Betroffene Daten                          | Backup                                                      | Wiederherstellung                                                                           |
|----------|----------------------------------------------------------------------------------|-------------------------------------------|-------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| WhatsApp | Nachrichtensofortverstand (Text, Foto, Video, Ton) mit Freunden, Familie und Arbeitskollegen | Chatgespräche, Fotos, Videos, Tolle Memes | Im App Konfiguriert: Google Drive Backup mit Passwortschutz | Bei der Installation der App werde ich gefragt, ob ich meine Daten wiederherstellen möchte. |

## Produkt
Das Produkt dieser Aufgabe ist eine Tabelle (Excel, Markdown, o.ä.), die anschliessend mit der Lehrperson besprochen wird (Teil der Leistungsbeurteilung).