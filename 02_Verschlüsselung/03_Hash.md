# Hash(funktion)
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Der Hashwert einer Datei berechnen.  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1G: Kann das Prinzip der Verschlüsselung von Daten erläutern |

Eine [Hashfunktion](https://de.wikipedia.org/wiki/Hashfunktion) ist eine Abbildung, die eine grosse Eingabemenge auf eine kleinere Zielmenge (den Hashwert) abbildet. Für die Datensicherheit sind vor allem die [kryptographische Hashfunktionen](https://de.wikipedia.org/wiki/Kryptographische_Hashfunktion) relevant. Diese Funktionen nehmen eine Bit- oder Bytefolge als Input und erzeugen daraus einen Hash von fixer Länge. Dabei haben diese Funktionen zwei wichtige Eigenschaften:
 - Kollisionsresistent: Es ist praktisch nicht möglich, zwei unterschiedliche Eingabewerte zu finden, die einen identischen Hash ergeben.
 - Einwegfunktion: Es ist praktisch nicht möglich, aus dem Hash den Eingabewert zu rekonstruieren.

Je nach Anwendungsbereich gelten unterschiedliche Anforderungen an die Hashfunktion. Die dabei in der praxis eingesetzten Verfahren ändern sich fortlaufend. Aktuell gilt folgende Empfehlung: 

| Zweck | Hashfunktion  |
|---|---|
| Überprüfung der Datenintegrität einer Datei  | SHA-256 (Mindestens MD5, SHA-1)  |
| Einsatz in Verschlüsselung (Container, VPN, usw.)  | Mindestens SHA-256, besser SHA-512, Whirlpool    |

## 3.1. Aufgabe - Überprüfung heruntergeladenen *installation images (ISO)*
Ein häufiger Einsatzzweck von Hashfunktionen ist die Überprüfung von Checksummen heruntergeladenen Dateien. Dabei wird sichergestellt, dass es sich bei der heruntergeladenen Datei zu keinen Fehlern bei der Übermittlung gekommen ist. 

Mithilfe von Powershell lässt sich dies sehr einfach überprüfen.

Der nachfolgende Befehl gibt für eine *DATEI* und einen *HASH* *True* aus, wenn der berechnete Hashwert der Datei mit dem Mitgebenden übereinstimmt oder *False*, falls das nicht der Fall ist. 

```powershell
(Get-FileHash DATEI -Algorithm SHA512).Hash -eq "HASH"
```

Beispiel für ```debian-11.2.0-amd64-netinst.iso```
```powershell
(Get-FileHash debian-11.2.0-amd64-netinst.iso -Algorithm SHA512).Hash -eq "c685b85cf9f248633ba3cd2b9f9e781fa03225587e0c332aef2063f6877a1f0622f56d44cf0690087b0ca36883147ecb5593e3da6f965968402cdbdf12f6dd74"
```

### 3.1.1. Videoanleitung - Hashwert einer Datei überprüfen
[Video - Cryptool - Hash](videos/hashoffile.webm)

## 3.2. Aufgabe - Gleicher Input gleicher Hash
Um ein besseres Gefühl für kryptografische Hashfunktionen zu erhalten, empfiehlt es sich verschiedene Hashfunktionen mit dem Cryptool auszuprobieren. Schauen Sie sich das nachfolgende Video an und versuchen Sie dasselbe mit anderen Hashwerten. Welche Hashwerte können Sie googlen, welche nicht?

[Video - Cryptool - Hash](videos/cryptoolhash.webm)
